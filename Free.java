public class Free extends Piece {

    // Constants
    private static final char FREE = '.';

    // Constructor
    public Free() {
        symbol = FREE;
    }

}
