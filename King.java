public class King extends Piece {

    // Constants
    protected static final char WHITEKING = '♔', BLACKKING = '♚';

    // Constructor
    public King(boolean color, Board board) {
        super(color, board);

        if (color)
            symbol = WHITEKING;
        else
            symbol = BLACKKING;
    }

    // Methods
    @ Override
    public boolean isValidPieceMove(int[] move) {
        // Store abs values of differences b/w columns and rows
        int absColDiff = Math.abs(move[0] - move[2]);
        int absRowDiff = Math.abs(move[1] - move[3]);

        // The king can only move one square in any direction
        return !(absColDiff > 1 || absRowDiff > 1);
    }

}
