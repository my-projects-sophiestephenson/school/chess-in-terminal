public class Pawn extends Piece {

    // Constants
    private static final char WHITEPAWN = '♙', BLACKPAWN = '♟';

    // Attributes
    private boolean firstMove;

    // Constructor
    public Pawn(boolean color, Board board) {
        super(color, board);

        if (color)
            symbol = WHITEPAWN;
        else
            symbol = BLACKPAWN;

        firstMove = true;
    }

    // Getters & Setters
    public boolean isFirstMove() {
        return firstMove;
    }
    public void takeFirstMove() {
        firstMove = false;
    }

    // Methods
    @Override
    public boolean isValidPieceMove(int[] move) {

        // Store differences (and absolute values) between indices in variables
        int colDiff = move[0] - move[2];
        int rowDiff = move[1] - move[3];
        int absColDiff = Math.abs(colDiff);

        // White pawns can only move up and black pawns can only move down,
        //   so we create a variable to reflect that
        int dir;
        if (color)
            dir = 1;
        else
            dir = -1;

        // The pawn can move in three ways:
        //  1. One square forward, if there's no piece there
        //  2. Two squares forward, only on the first move and if there's no piece in the way
        //  3. One square diagonally forward, only to capture a piece
        return  ((rowDiff == dir && colDiff == 0) ||
                 (rowDiff == (2 * dir) && colDiff == 0 && firstMove) ||
                 (rowDiff == dir && absColDiff == 1 &&
                        !(board.getPieceAtPos(move[2], move[3]) instanceof Free)));
    }

    @Override
    public boolean isPathClear(int[] move) {
        // Store for easy access
        int colDiff = move[0] - move[2], absColDiff = Math.abs(colDiff);
        int absRowDiff = Math.abs(move[1] - move[3]);

        // Direction depends on color
        int dir;
        if (color)
            dir = 1;
        else
            dir = -1;

                // Either it's moving diagonally, which we've already checked is okay, or it's moving forward.
                //    If so, the end spot must be free, and if it's moving more than one space up, the middle spot
                //    must also be free.
        return  absColDiff == 1 ||
                (board.getPieceAtPos(move[2], move[3]) instanceof Free) &&
                        (absRowDiff == 1 || board.getPieceAtPos(move[0], move[1] - dir) instanceof Free);
    }

}
