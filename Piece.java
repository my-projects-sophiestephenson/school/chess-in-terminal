public class Piece {

    // Attributes
    protected boolean color;  // White = true, black = false
    protected char    symbol; // The piece's symbol to be printed to the board
    protected Board   board;  // The board the piece is on


    // Constructors
    public Piece() {}
    public Piece(boolean color, Board board) {
        this.color = color;
        this.board = board;
    }

    // Getters & Setters
    public char getSymbol() {
        return symbol;
    }
    public boolean getColor() { return color; }
    public Board getBoard() { return board; }

    // Methods
    public boolean isValidPieceMove(int[] move) { return false; }
    public boolean isPathClear(int[] move) {
        return true;
    }

}
